import json
import os, sys

path = "C:\\java-neon\\RCIDetector\\results"
dirs = os.listdir(path)
json_files = []

multidex = []
runtimeexec = []
nativelibrary = []
completed = []

for file in dirs:
    if '.json' in file:
        json_files.append(file)

def check(filename):
    with open(filename) as json_file:
        json_data = json.load(json_file)
        
        #print json_data["TriggerPoints"]["Multidex"]
        #print json_data["TriggerPoints"]["RuntimeExec"]
        #print json_data["TriggerPoints"]["NativeLibrary"]

        if json_data["TriggerPoints"]["Multidex"] == 1:
            multidex.append(filename)
            
        if json_data["TriggerPoints"]["RuntimeExec"] == 1:
            runtimeexec.append(filename)
        if json_data["TriggerPoints"]["NativeLibrary"] == 1:
            nativelibrary.append(filename)
        
        if "Completed" in json_data:
            #print json_data["Completed"]
            if json_data["Completed"] == 1:
                completed.append(filename)

        

for file in json_files:
    check(file)

print "Trigger Points:"
print "\tMultidex: " + str(len(multidex))
print "\tRuntimeExec: " + str(len(runtimeexec))
print "\tNativeLibrary: " + str(len(nativelibrary))
print "Completed: " + str(len(completed)) + "/" + str(len(json_files)) + "(" + str(100.0 * len(completed)/len(json_files)) + "%)"
