package detector;

import java.io.PrintStream;

import detector.constants.Constants;
import detector.infoflow.BSInterproceduralTransformer;
import detector.infoflow.CFGTransformer;
import detector.infoflow.SourceSink;
import soot.Pack;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.Transform;
import soot.options.Options;

public class Test {
	public final static PrintStream ps = System.out;
	
	public static void main(String[] args) {		
		if(args.length == 0) {
			System.err.print("No apk path. Exit!\n");
			System.exit(1);
		}
		
		String apkpath = args[0];
		System.out.println("Analyzing:"+apkpath);
		
		// Get the sources and sinks from the input files
		SourceSink.extractSootSourceSink();
		
		{
			SceneTransformer cfgTransformer = new CFGTransformer(args[0]);
			Pack pack = PackManager.v().getPack("cg");
			pack.add(new Transform("cg.mtran", cfgTransformer));
			
			PackManager.v().getPack("wjtp").add(new Transform("wjtp.inter", BSInterproceduralTransformer.v()));
						
			String[] sootArgs = {"-w","-f", "n", "-allow-phantom-refs", "-x", "android.support.", "-x", "android.annotation.",
					"-process-dir", args[0], "-android-jars", Constants.ANDROID_JARS, "-src-prec", "apk", "-no-bodies-for-excluded"	};
			
			Scene.v().addBasicClass("android.support.v4.widget.DrawerLayout",SootClass.BODIES);
			Scene.v().addBasicClass("org.apache.http.client.utils.URLEncodedUtils",SootClass.SIGNATURES);
			Scene.v().addBasicClass("org.apache.http.protocol.BasicHttpContext",SootClass.HIERARCHY);
			
			Options.v().set_output_dir("H://Android//RCIDetector//detector//sootOutput");
			//PackManager.v().writeOutput();
			
			soot.Main.main(sootArgs);
			System.out.println("- END -");
		}
			
	}
	
	public static void println(Object o){
		ps.println(o);
		System.setOut(ps);
	}
}
