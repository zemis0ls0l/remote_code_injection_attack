package detector.utils;

import java.util.Collections;

import soot.Main;
import soot.PackManager;
import soot.PhaseOptions;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.options.Options;

public class JimpleLoader {
	
	public JimpleLoader(String jimple) {
		
		// Parsing .jimple files
		Options.v().set_src_prec(Options.src_prec_jimple);
		//ex) jimple = "G://Android//Season2//Dataset//candidates//com.waycreon.kewltv_xbmc_updater.apk//";
		Options.v().set_process_dir(Collections.singletonList(jimple));
				
		Options.v().set_android_jars("H://Android//FlowDroid//android-platforms//");
				
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_whole_program(true);
		Options.v().set_no_bodies_for_excluded(true);
				
		//Options.v().set_exclude(Arrays.asList("android.support"));
				
		PhaseOptions.v().processPhaseOptions("cg.spark", "enabled:true");
				
		Scene.v().loadNecessaryClasses();
		PackManager.v().runPacks();
		Main.v();
	}
}
