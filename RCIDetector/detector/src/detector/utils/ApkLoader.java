package detector.utils;

import java.util.Collections;

import soot.Main;
import soot.PackManager;
import soot.PhaseOptions;
import soot.Scene;
import soot.options.Options;

public class ApkLoader {

	public ApkLoader(String apk) {
				
		// Parsing .apk files
		Options.v().set_src_prec(Options.src_prec_apk);
		// ex) apk = "H://Android//Test//com.runtastic.android.apk//com.runtastic.android.apk"
		Options.v().set_process_dir(Collections.singletonList(apk));
		Options.v().set_android_jars("H://Android//FlowDroid//android-platforms//");

		Options.v().set_output_format(Options.output_format_jimple);
		// if output needed,
		//Options.v().set_output_dir("H://Android//RCIDetector//detector//sootOutput");
		
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_whole_program(true);
		Options.v().set_no_bodies_for_excluded(true);
				
		//Options.v().set_exclude(Arrays.asList("android.support"));
				
		PhaseOptions.v().processPhaseOptions("cg.spark", "enabled:true");
				
		Scene.v().loadNecessaryClasses();
		// if transformation needed,
		//PackManager.v().getPack("wjtp");
		PackManager.v().runPacks();
		// if output needed,
		//PackManager.v().writeOutput();
		
		Main.v();
	}
}
