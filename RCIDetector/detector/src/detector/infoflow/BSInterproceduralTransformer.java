package detector.infoflow;

import java.util.Iterator;
import java.util.Map;

import detector.BSG.BlueSealGraph;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.toolkits.scalar.ArraySparseSet;

public class BSInterproceduralTransformer extends SceneTransformer {
	
	private static BSInterproceduralTransformer instance = new BSInterproceduralTransformer();

	public BSInterproceduralTransformer() {
	}
	
	public static BSInterproceduralTransformer v() {
		return instance;
	}
	
	@Override
	protected void internalTransform(String arg0, Map<String, String> arg1) {
        CallGraph cg = CFGTransformer.cg;
        
		BSInterproceduralAnalysis inter = new BSInterproceduralAnalysis(
				cg, new SootMethodFilter(null), CFGTransformer.reachableMethods_.iterator(), false);
		
		// (DEBUG)
		/*
		Map<SootMethod, BlueSealGraph> test = inter.data;
		for(SootMethod u : test.keySet()) {
			System.out.println(u.toString());
			BlueSealGraph fs = test.get(u);
			if (u.toString().contains("GetResponse"))
				fs.print();
		}
		System.exit(0);
		*/
		
		/**
		 * @param methodSummary
		 * @param entryMethod
		 * @param isForward
		 */
		//GlobalBSG gBSG = new GlobalBSG(inter.data, CFGTransformer.reachableMethods_.iterator(), true);
		//gBSG.print();
		//System.exit(0);		
		
		/*
		 * simple backward slicing by zemisolsol
		 */
		// 1. find start point
		// 2. call stack based backward tracking
		
		//
		Map<SootMethod, Map<Unit, ArraySparseSet>> results = inter.getMethodSummary();
		for (SootClass sc : Scene.v().getClasses()) {
			for (SootMethod sm : sc.getMethods()) {
				if (sm.toString().contains("doInBackground") && sm.toString().contains("com.example")) {
					Map<Unit, ArraySparseSet> intraFlowSet = results.get(sm);
					for(Unit u : intraFlowSet.keySet()) {
						System.out.println("[FlowSet] "+u.toString());
						ArraySparseSet fs = intraFlowSet.get(u);
						Iterator itr = fs.iterator();
						while(itr.hasNext())
					         System.out.println("\t-> "+itr.next().toString());
					}
					
					BlueSealGraph intraNodeSet = (BlueSealGraph) inter.getSummaryFor(sm);
					intraNodeSet.print();
				}
			}
		}
		System.exit(0);
		//
		
		
		// redirect results to file (/results/output.txt)
		/*
		PrintStream stdout = System.out;
		try {
			System.setOut(new PrintStream("H://Android//RCIDetector//detector//results//output.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		gBSG.print();
		// reset stdout
		System.setOut(stdout); 
		*/
	}

}
