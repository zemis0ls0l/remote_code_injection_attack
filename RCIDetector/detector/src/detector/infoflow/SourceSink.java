package detector.infoflow;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SourceSink {

	public static Map<String, ArrayList<String>> sources_ = new HashMap<String, ArrayList<String>>();
	public static Map<String, ArrayList<String>> sinks_ = new HashMap<String, ArrayList<String>>();
	public static Set<String> CPSrcStrings = new HashSet<String>();
	public static Map<String, ArrayList<String>> CPSrcFields = new HashMap<String, ArrayList<String>>();

	public static void extractSootSourceSink() {		
		sources_ = extractSourcesAndSinks("input/Sources.txt");		
		sinks_ = extractSourcesAndSinks("input/Sinks.txt");
	}

	/*
	 * Parse Sources.txt and Sinks.txt file
	 * ex) class: java.net.URLConnection
	 *     getInputStream
	 */
	@SuppressWarnings("resource")
	public static Map<String, ArrayList<String>> extractSourcesAndSinks(String path) {
		Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();

		try {
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader iReader = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(iReader);
			String line;
			String className = null;

			while ((line = br.readLine()) != null) {
				if (line.startsWith("#")) {
					continue;
				}
				
				if (line.startsWith("class: ")) {
					className = line.substring(7);
					continue;
				}

				if (!map.containsKey(className)) {
					ArrayList<String> tempList = new ArrayList<String>();
					tempList.add(line);
					map.put(className, tempList);
				} else {
					map.get(className).add(line);
				}
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
}
