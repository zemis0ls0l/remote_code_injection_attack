package detector.infoflow;

import java.util.ArrayList;

import soot.SootClass;
import soot.SootMethodRef;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;

public class SourceSinkDetection {
	
	public static String contentprovider;
	
	public SourceSinkDetection() {
	}
	
	public static boolean isSource(Stmt stmt) {
		if (stmt.containsInvokeExpr()) {
			SootMethodRef methodRef = stmt.getInvokeExpr().getMethodRef();
			String decClass = methodRef.declaringClass().getName();
			for (String sourceClassStr: SourceSink.sources_.keySet()) {
				if (sourceClassStr.equals(decClass)) {
					ArrayList<String> methodList = SourceSink.sources_.get(sourceClassStr);
					for (String method : methodList) {
						if (methodRef.name().contains(method))
							return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public static boolean isSink(Stmt stmt) {
		if (stmt.containsInvokeExpr()) {
			SootMethodRef methodRef = stmt.getInvokeExpr().getMethodRef();
			String decClass = methodRef.declaringClass().getName();
			for (String sourceClassStr: SourceSink.sinks_.keySet()) {
				if (sourceClassStr.equals(decClass)) {
					ArrayList<String> methodList = SourceSink.sinks_.get(sourceClassStr);
					for (String method:methodList) {
						if (methodRef.name().contains(method))
							return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public static boolean isCPSource(Stmt stmt) {
		if(stmt.containsInvokeExpr()){
			InvokeExpr parseie = stmt.getInvokeExpr();
			 SootClass defClass = parseie.getMethodRef().declaringClass();
			 String mName = parseie.getMethodRef().name();
			 if(defClass.getName().equals("android.net.Uri")&&mName.equals("parse")){
				 //get the argument
				 Value arg = parseie.getArg(0);
				 if(arg instanceof StringConstant){
					 if(SourceSink.CPSrcStrings.contains(arg.toString().substring(1, arg.toString().length()-1))){
						 contentprovider = arg.toString().substring(1, arg.toString().length()-1);
						 return true;
					 }
				 }
			 }
		}
		if(!(stmt instanceof AssignStmt)) return false;
		
		Value value = ((AssignStmt)stmt).getRightOp();
		
		if(value instanceof StaticFieldRef){ 
			if(SourceSink.CPSrcStrings.contains(value.toString())){
				contentprovider = value.toString();
				return true;
			}
		}
		return false;
	}
	
	public static boolean isCPQuery(Stmt stmt) {
		if (stmt.containsInvokeExpr()) {
			SootMethodRef methodRef = stmt.getInvokeExpr().getMethodRef();
			SootClass definedClass = methodRef.declaringClass();
			String methodName = methodRef.name();
			if (definedClass.getName().equals("android.content.ContentResolver")
					|| definedClass.getName().equals("android.content.ContentProviderClient")) {
				if (methodName.equals("query")) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isCPSink(Stmt stmt){
		if (stmt.containsInvokeExpr()) {
			SootMethodRef methodRef = stmt.getInvokeExpr().getMethodRef();
			
			if (methodRef.declaringClass().getName().contains("ContentResolver")
					|| methodRef.declaringClass().getName().contains("ContentProviderClient")) {
				if (methodRef.name().contains("insert")
						|| methodRef.name().contains("update")) {
					return true;
				}
			}
		}
		
		return false;
	}
}
