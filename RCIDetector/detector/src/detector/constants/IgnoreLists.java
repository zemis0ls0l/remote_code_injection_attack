package detector.constants;

public class IgnoreLists {
	public static boolean isClassInSystemPackage(String className) {
		return className.startsWith("android.")
				|| className.startsWith("java.")
				|| className.startsWith("sun.");
	}
}
