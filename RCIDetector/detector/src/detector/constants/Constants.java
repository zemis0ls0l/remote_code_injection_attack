package detector.constants;

public class Constants {
	
	public final static String ROOT = "H:/Android/RCIDetector/detector";
	public final static String TOOLS = "./tools/";
	public final static String ANDROID_JARS = "H:/Android/RCIDetector/android-platforms";
	public final static String apktool = TOOLS + "apktool";
	
}
